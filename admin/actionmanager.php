<?php

require_once("inc_dbfunctions.php");
require_once("config.php");

$actionmanager = New ActionManager();

if(isset($_POST['command']) && $_POST['command'] == 'members_login')
{
    $actionmanager->members_login();
}

class ActionManager
{
    function members_login()
    {
        $mycon = databaseConnect();
        $username = $_POST['username'];
        $password = $_POST['password'];
        $thedate = date("Y-m-d H:i:s");
        
        $dataread = New DataRead();
        $dataWrite = New DataWrite();
        
        //generate the encoded password
        $password = generatePassword($password);
        
        //check whether the email and password exists
        $member_get = $dataread->member_getbyusernamepassword($mycon, $username, $password);
        if(!$member_get)
        {
            echo "<script type='text/javascript'>
                    console.log('hello');
                    showError('Invalid ID and password');
                    </script>";
            return;
        }
       if ($member_get['status'] == '0')
        {
            echo "<script type='text/javascript'>
                   showError('Your account is temporary disabled.');
                    </script>";
            return;
        }
        
        
        
        createCookie("userid",$member_get['member_id']);
        createCookie("userlogin","YES");
        createCookie("adminlogin", "NO");
        createCookie("username",$member_get['username']);
        createCookie("email", $member_get['email']);
        createCookie("fullname",$member_get['firstname']." ".$member_get['lastname']);
        
         echo "<script type-'text/javascript'>
                    showSuccess('Login successful...');
                    window.setTimeout(function(){
                        document.location.href='dashboard.php';
                    },2000);
                </script>";
        return;
    }
}

?>