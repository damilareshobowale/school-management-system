(function(window, undefined) {
  'use strict';

  /*
  NOTE:
  ------
  PLACE HERE YOUR OWN JAVASCRIPT CODE IF NEEDED
  WE WILL RELEASE FUTURE UPDATES SO IN ORDER TO NOT OVERWRITE YOUR JAVASCRIPT CODE PLEASE CONSIDER WRITING YOUR SCRIPT HERE.  */

  //LOGIN r
  $('#loginfom').on("submit", function(event){
    $('#loginbutton').html("Signing on...");
    $('.error').removeClass("has-error");


    event.preventDefault();

     /* get some values from elements on the page: */
         var $form = $(this),
            username = $form.find('input[name="username"]').val(),
            password = $form.find('input[name="password"]').val(),
            url = $form.attr('action');

     //handle validations
     var $msg = '';
     var count = 0;
     if (username == '')
     {
        $('#usernamediv').addClass("has-error")
        $msg += 'Username is required <br />';
        count += 1;
     }
     if (password == '')
     {
        $('#passworddiv').addClass("has-error")
        $msg += 'Password is required<br />';
        count += 1;
     }
    
     if ($msg != '')
     {
		$('#result').html(showError($msg));
        $('#loginbutton').html("<i class='ft-unlock'></i> Login");
        return;
     }


     /* Send the data using post */
    var posting = $.post(url, {
        username: username,
        password: password,
        command: "members_login" 
    });
    
    /* Put the results in a div */
    posting.done(function(data) {
        $("#result").html(data);
        $('#loginbutton').html("<i class='ft-unlock'></i> Login");


                    
    });
       
       return;                 

});

})(window);