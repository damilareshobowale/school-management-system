//Error Message
function showError($message)
{
	$error = "<div class='alert alert-icon-left alert-danger alert-dismissible mb-2' role='alert'>" +
                          "<span class='alert-icon'><i class='la la-warning'></i></span>" +
                          "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>" +
                            "<span aria-hidden='true'>&times;</span>" +
                          "</button>" + $message +
                        "</div>";

    return $error;
}


//Success Message
function showSuccess($message)
{
	$error = "<div class='alert alert-icon-left alert-success alert-dismissible mb-2' role='alert'>" +
                          "<span class='alert-icon'><i class='la la-thumb-up'></i></span>" +
                          "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>" +
                            "<span aria-hidden='true'>&times;</span>" +
                          "</button>" + $message +
                        "</div>";

    return $error;
}